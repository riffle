import os
from datetime import datetime, timedelta

media_dir = "./media"
device_dir = "/media/IPOD/Podcasts" 

if not os.path.exists(device_dir):
    device_dir = "./device"
    if not os.path.exists(device_dir):
        os.makedirs(device_dir)

# consider timestamps earlier then this not interesting
reasonable_timestamp = datetime(2008,1,1) 

# do not request feeds more often then this period
min_ping_period = timedelta(minutes=30)

# initial value for timestamps more convenient then None
dawn_of_time = datetime(1999,1,1)
