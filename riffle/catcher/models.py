from __future__ import with_statement
from contextlib import contextmanager

from django.db import models

import conf

import logging
import sys
import os
from datetime import datetime, timedelta
import time
import urllib2, urlparse
from email.utils import parsedate
import feedparser
from subprocess import Popen

# FIXME this deserves to go to util
@contextmanager
def url_open(url):
    connection = urllib2.urlopen(url)
    yield connection
    connection.close()

def url_basename(url):
    return os.path.basename( urlparse.urlparse(url)[2] )

class Feed(models.Model):
    url = models.CharField(maxlength=1024,unique=True)
    timestamp = models.DateTimeField(null=True,default=conf.dawn_of_time)
    ping_timestamp = models.DateTimeField(default=conf.dawn_of_time)
    title = models.CharField(maxlength=1024,blank=True,default='')
    class Admin: pass

    def __str__(self):
        return self.title if self.title != '' else self.url

    def refresh(self):
        if not self.may_ping():
            return
        logging.info("Refreshing %s", self)
        try:
            with url_open(self.url) as conn:
                self.ping_timestamp = datetime.now()
                remote_timestamp = self.assert_updated(conn)
                logging.debug("Feed '%s' might have changed, parsing", self)
                tree = feedparser.parse(conn)
                try: 
                    if self.title != tree['feed']['title']:
                        logging.debug("Feed title has changed from '%s' to '%s'",
                                self.title, tree['feed']['title'])
                        self.title = tree['feed']['title']
                except: 
                    pass
                watermark = self.watermark()
                logging.debug('Feed watermark: %s', watermark)
                for e in tree.entries:
                    timestamp = datetime.fromtimestamp(
                            time.mktime(e.modified_parsed))
                    if timestamp > watermark and 'enclosures' in e:
                        for encl in e.enclosures:
                            url = encl.href
                            if Episode.objects.filter(url=url).count() > 0:
                                logging.debug('Episode %s already injected', url)
                                continue

                            logging.debug('Injecting episode %s', url)
                            new_episode = Episode.objects.create(
                                    feed = self,
                                    url = url,
                                    local_path = Episode.make_local_path(url),
                                    timestamp = timestamp)
                self.timestamp = remote_timestamp
        except Feed.NotChanged:
            logging.debug("Feed '%s' haven't changed, skipping", self)
        except:
            logging.error('%s while trying to refresh %s', sys.exc_value, self)
        finally:
            # always persist: at least to save ping_timestamp
            self.save()

    class NotChanged: 
        "This will be raised if feed havent updated"
        pass

    def assert_updated(self, conn):
        remote_timestamp = None
        if 'Last-Modified' in conn.info():
            remote_timestamp = datetime.fromtimestamp( 
                    time.mktime(
                        parsedate( conn.info()['Last-Modified'] )))
        logging.debug('comparing local(%s) against remote(%s) timestamps',
                self.timestamp,
                remote_timestamp)
        if conf.opts.force_refresh \
                and self.timestamp is not None \
                and remote_timestamp is not None \
                and remote_timestamp > conf.reasonable_timestamp \
                and remote_timestamp <= self.timestamp:
                    raise Feed.NotChanged()
        return remote_timestamp


    def may_ping(self):
        if conf.opts.force_refresh:
            logging.debug('--force-dl detected: ignoring ping period')
            return True
        else:
            may = (datetime.now() - self.ping_timestamp) \
                    > conf.min_ping_period
            logging.debug('May%s ping %s', '' if may else ' not', self)
            return may

    def watermark(self):
        """timestamp of the latest injected episode"""
        try:
            return self.episodes.order_by('-timestamp')[0].timestamp
        except IndexError:
            return datetime.now() - timedelta(days=60) # FIXME should be in conf 

class Episode(models.Model):
    feed = models.ForeignKey(Feed,edit_inline=models.TABULAR,related_name='episodes')
    url = models.CharField(maxlength=1024,unique=True,core=True)
    local_path = models.CharField(maxlength=1024,unique=True)
    timestamp = models.DateTimeField()
    downloaded = models.DateTimeField(null=True,default=None)

    def __str__(self):
        return "%s: %s" % (self.feed, self.local_path)

    def download(self):
        logging.info('Downloading %s', self)
        fname = os.path.join( conf.media_dir, self.local_path )
        dl_fname = fname + ".part"

        # -c is continue partial download
        cmd = "wget -c '%s' -O '%s'" % (self.url, dl_fname)
        logging.debug("Exec: %s", cmd)
        if Popen(cmd, shell=True).wait() == 0:
            logging.debug("Successfully downloaded %s, renaming to %s", 
                    self.url, self.local_path)
            os.rename(dl_fname, fname)
            self.downloaded = datetime.now()
            self.save()
        else:
            logging.warning('Failed to download %s', self)

    @classmethod
    def make_local_path(cls,url):
        wanted = url_basename(url)
        wanted_base, wanted_ext = os.path.splitext(wanted)
        # these are but *potential* clashes
        clashes = [e.local_path for e in 
                Episode.objects.filter(local_path__startswith = wanted_base,
                                       local_path__endswith = wanted_ext)]
        mod = 1
        if wanted in clashes:
            logging.debug('local_path %s already exists, inventing new',wanted)
            while wanted in clashes:
                wanted = wanted_base + str(mod) + wanted_ext
                mod += 1
        logging.debug('local_path for %s is %s', url, wanted)
        return wanted
        
