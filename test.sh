#!/bin/bash

python shuffle.py 01-test copy > read.log \
  && diff -u {old,read}.log \
  && python shuffle.py copy > read-copy.log \
  && diff -u read{,-copy}.log
