"""
iPod Shuffle database access

File format documentation:
- http://ipodlinux.org/ITunesDB#iTunesSD_file and further

Author: Artem Baguinski
"""

from __future__ import with_statement
import os, sys
from records import *

READ = 'rb'
WRITE = 'w+b'

iTunesSD_track = Record([
        Field(Uint24(), '%reclen%', check = True),
        Skip(3),
        Field(Uint24(), 'starttime', default = 0),
        Skip(6),
        Field(Uint24(), 'stoptime', default = 0),
        Skip(6),
        Field(Uint24(), 'volume', default = 0x64),
        # for the following fileds there is no sensible default
        Field(Uint24(), 'file_type'),
        Skip(3),
        Field(ZeroPaddedString(522, 'UTF-16-LE'), 'filename'),
        Field(Bool8(), 'shuffleflag'),
        Field(Bool8(), 'bookmarkflag'),
        Skip(1)])

iTunesSD = Record([
    Record([
        Field(Uint24(), 'len(tracks)'),
        Field(Uint24(), const = 0x010800),
        Field(Uint24(), '%reclen%', check = True),
        Skip(9)]),
    ListField('tracks', iTunesSD_track)],
    BIG_ENDIAN)

iTunesStats_track = Record([
        Field( Uint24(), '%reclen%', check = True),
        Field( Int24(), 'bookmarktime', default = -1),
        Skip(6),
        Field( Uint24(), 'playcount', default = 0),
        Field( Uint24(), 'skippedcount', default = 0)])

iTunesStats = Record([
    Record([
        Field( Uint24(), 'len(tracks)'),
        Skip(3)]),
    ListField('tracks', iTunesStats_track)],
    LITTLE_ENDIAN)

# Here I disagree with the format from URL, but I'm not sure about nothing
iTunesPState = Record([
    Field( Uint8(), 'volume', default = 29 ),
    Field( Uint24(), 'shufflepos', default = 0 ),
    Field( Uint24(), 'trackno', default = 0 ),
    Field( Bool24(), 'shuffleflag', default = False),
    Field( Uint24(), 'trackpos', default = 0),
    Skip(19)],
    LITTLE_ENDIAN)

class ShuffleException(Exception):
    pass

class ShuffleDB:
    supported_file_types = (".mp3", ".aa", ".m4a", ".m4b", ".m4p", ".wav")

    def write_iTunesSD(self, tracks):
        with open('iTunesSD', WRITE) as file:
            iTunesSD.write(file, {'tracks':tracks})
            file.truncate()

    def read_iTunesSD(self):
        with open('iTunesSD', READ) as file:
            return iTunesSD.read(file)['tracks']

    def write_iTunesStats(self, tracks):
        with open('iTunesStats', WRITE) as file:
            iTunesStats.write(file, {'tracks':tracks})
            file.truncate()

    def read_iTunesStats(self, tracks):
        with open('iTunesStats', READ) as file:
            iTunesStats.read(file,{'tracks':tracks})['tracks']

    def write_iTunesPState(self, pstate):
        with open('iTunesPState', WRITE) as file:
            iTunesPState.write(file, pstate)
            file.truncate()

    def read_iTunesPState(self):
        with open('iTunesPState', READ) as file:
            return iTunesPState.read(file)
    
    def read_all(self, control_dir):
        cwd = os.getcwd()
        os.chdir(control_dir)
        try:
            tracks = self.read_iTunesSD()
            self.read_iTunesStats(tracks)
            pstate = self.read_iTunesPState()
            return (tracks, pstate)
        except FormatError:
            return ([], iTunesPState.make_default())
        finally:
            os.chdir(cwd)

    def write_all(self, control_dir, tracks, pstate):
        cwd = os.getcwd()
        os.chdir(control_dir)
        try:
            self.write_iTunesSD(tracks)
            self.write_iTunesStats(tracks)
            self.write_iTunesPState(pstate)
            if os.path.exists("iTunesShuffle"):
                os.remove("iTunesShuffle") # let the player recreate it
        finally:
            os.chdir(cwd)

    def set_old_tracks(self, lst):
        self.old_tracks = {}
        for i in xrange(len(lst)):
            t = lst[i]
            self.old_tracks[t['filename']] = t
            self.old_tracks[i] = t

    def make_track_record(self, filename):
        if self.old_tracks.has_key(filename):
            return self.old_tracks[filename]
        else:
            dict = {}
            iTunesSD_track.make_default(dict)
            iTunesStats_track.make_default(dict)

            dict['filename'] = filename
            if filename.endswith((".mp3",".aa")):
                dict['file_type'] = 1
            elif filename.endswith((".m4a", ".m4b", ".m4p")):
                dict['file_type'] = 2
            elif filename.endswith(".wav"):
                dict['file_type'] = 4
            else:
                raise ShuffleException(
                        "%s: unsupported file type" % (filename))
            dict['bookmarkflag'] = filename.endswith((".aa",".m4b"))
            dict['shuffleflag'] = not dict['bookmarkflag']
            return dict

#####################################################################
if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.DEBUG,filename='shuffle-debug.log')

    def print_list(xs):
        for x in xs:
            print x

    if len(sys.argv) > 1:
        # try reading
        db = ShuffleDB()
        tracks, pstate = db.read_all(sys.argv[1])
        print_list( tracks )
        print pstate
       
    #if len(sys.argv) > 2:
        # try writing
        #os.chdir(sys.argv[2])
        #db.write_all(tracks, pstate)

