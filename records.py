import struct, os, re

from logging import debug

BIG_ENDIAN = True
LITTLE_ENDIAN = False

class RecordsException(Exception):
    pass

class FormatError(RecordsException):
    pass

class UnknownSizeException(RecordsException):
    pass

### Fields
class BaseField:
    def set_bigendian(self, ignore): pass
    def set_reclen(self, ignore): pass
    def put_default(self, dict): pass

###### Skip is a dummy field that isn't read
class Skip(BaseField):
    def __init__(self, n): 
        self.get_size = (lambda:n)
        self.write = self.read
    def read(self, file, dict): 
        file.seek(self.get_size(), os.SEEK_CUR)

###### Field is a composite field object factory
class Field(BaseField):
    # Value handlers - know what to do with read values and
    # where to get values to be written
    class Named:
        def __init__(self, name, default): 
            self.name = name
            self.default = default
        def get(self, dict): 
            return dict[self.name] if dict.has_key(self.name) else self.default
        def put(self, dict, value): 
            dict[self.name] = value
        def put_default(self, dict): 
            dict[self.name] = self.default
        def __str__(self): return self.name

    class LenLookup:
        re = re.compile("len\(([^)]+)\)")
        def __init__(self, expr):
            self.expr = expr
            self.array_name = self.re.match(expr).group(1)
        def get(self, dict): 
            return len( dict[self.array_name] )
        def put(self, dict, value): 
            dict[self.expr] = value
        def put_default(self, dict): pass
        def __str__(self): return self.expr

    class Const:
        def __init__(self, const, check):
            self.const = const
            if check: 
                self.put = self.check
        def get(self, dict): 
            return self.const
        def check(self, dict, value):
            if value != self.const: 
                raise FormatError("Expected %s, got %s" % (self.const, value))
        def put(self, dict, value): pass
        def put_default(self, dict): pass
        def __str__(self): return "Constant(%s)" % self.const

    # Field factory - composes field from packer and value handler
    def __init__(self, packer, name=None, default=None, const=None, check = False):
        # helpers
        def set_value_handler(vh):
            self.put = vh.put
            self.get = vh.get
            self.put_default = vh.put_default
            self.__str__ = vh.__str__
        def const_later(const):
            set_value_handler(Field.Const(const, check))

        if name == '%reclen%':
            self.set_reclen = const_later
        elif name is not None:
            if Field.LenLookup.re.match(name) is not None:
                set_value_handler(Field.LenLookup(name))
            else:
                set_value_handler(Field.Named(name,default))
        elif const is not None:
            set_value_handler(Field.Const(const, check))
        else:
            raise RecordsException("Bad field parameters")

        self.get_size = packer.get_size
        self.pack = packer.pack
        self.unpack = packer.unpack
        if packer.__class__.__dict__.has_key('set_bigendian'):
            self.set_bigendian = packer.set_bigendian

    def read(self, file, dict):
        value = self.unpack( file.read( self.get_size() ))
        debug("read %s: %s", self, value)
        self.put(dict, value)
    def write(self, file, dict):
        value = self.get(dict)
        debug("write %s: %s", self, value)
        file.write( self.pack( value ))

class ListField(BaseField):
    def __init__(self, name, cell):
        self.cell = cell
        self.name = name

    def read(self, file, dict):
        lst = dict[self.name] if dict.has_key(self.name) else []
        n = dict["len(%s)" % self.name]
        for i in xrange(0, n):
            if i < len(lst):
                lst[i] = self.cell.read(file, lst[i])
            else:
                lst.append(self.cell.read(file, {}))
        dict[self.name] = lst

    def write(self, file, dict):
        lst = dict[ self.name ]
        for c in lst:
            self.cell.write( file, c )

    def get_size(self): 
        raise UnknownSizeException()

    def set_bigendian(self, bigendian):
        self.cell.set_bigendian(bigendian)

### Packers
class SimplePacker:
    def __init__(self, fmt):
        self.fmt = fmt
        self.size = struct.calcsize(fmt)
    def pack(self,val): return struct.pack(self.fmt,val)
    def unpack(self,str): return struct.unpack(self.fmt,str)[0]
    def get_size(self): return self.size

class Uint8(SimplePacker):
    def __init__(self): SimplePacker.__init__(self,"B")

class Bool8(Uint8):
    def pack(self, val): return Uint8.pack(self, (1 if val else 0))
    def unpack(self, str): return Uint8.unpack(self, str) != 0

class Uint24:
    def __init__(self, bigendian = LITTLE_ENDIAN):
        self.bigendian = bigendian
    def get_size(self): return 3
    def pack(self, i):
        if self.bigendian:
            return struct.pack(">I",i)[1:4]
        else:
            return struct.pack("<I",i)[0:3]
    def unpack(self, s):
        if self.bigendian:
             return struct.unpack('>I','\x00' + s[0:3])[0]
        else:
            return struct.unpack('<I',s[0:3] + '\x00')[0]
    def set_bigendian(self, bigendian):
        self.bigendian = bigendian

class Int24(Uint24):
    def __init__(self, bigendian = LITTLE_ENDIAN):
        self.bigendian = bigendian
    def pack(self, i):
        if self.bigendian:
            return struct.pack(">i",i)[1:4]
        else:
            return struct.pack("<i",i)[0:3]
    def unpack(self, s):
        u = Uint24.unpack(self,s)
        if (u & 0x800) != 0:
            return - ((~u + 1) & 0xfff)
        else:
            return u

class Bool24(Int24):
    def __init__(self): Int24.__init__(self)
    def pack(self, val): return Int24.pack(self, (-1 if val else 0))
    def unpack(self, str): return Int24.unpack(self, str) != 0

class ZeroPaddedString:
    def __init__(self, len, enc):
        self.size = len
        self.enc = enc
    def pack(self, val):
        return val.encode(self.enc).ljust(self.size,'\x00')
    def unpack(self, str):
        if len(str) != self.size:
            raise FormatError()
        return str.decode(self.enc).rstrip('\x00')
    def get_size(self): return self.size

### Record - an ordered list of fields
class Record(BaseField):
    def __init__(self, fields, bigendian=None):
        self.fields = fields
        try:
            reclen = self.get_size()
            for f in fields:
                f.set_reclen(reclen)
        except UnknownSizeException:
            pass
        if bigendian is not None:
            self.set_bigendian(bigendian)

    def read(self, file, dict=None):
        if dict is None: dict = {}
        for f in self.fields:
            f.read(file, dict)
        return dict

    def write(self, file, dict):
        for f in self.fields:
            f.write(file, dict)

    def make_default(self, dict = None):
        if dict is None: 
            dict = {}
        for f in self.fields:
            f.put_default(dict)
        return dict

    def get_size(self):
        size = 0
        for f in self.fields:
            size += f.get_size()
        return size
    
    def set_bigendian(self, bigendian):
        for f in self.fields:
            f.set_bigendian(bigendian)

