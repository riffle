
def riffle(lol):
    """
    given list of lists return flat list of elements, riffled:
    - order of individual lists is preserved
    - elements of the same list are far apart

    (this really should be called "false riffle" because 
    it doesn't shuffle, or "perfect riffle" for the same 
    reason. consult youtube for the card shuffle technique 
    that inspired the name)
    """
    def merge(a, b):
        "does not modify either, returns merged list"
        if len(a) == 0:
            return b
        elif len(b) == 0:
            return a
        elif len(a) > len(b):
            x = a
            a = b
            b = x
        c = []
        r = float(len(a)) / len(b)
        i_a = 0
        for i_b in xrange(len(b)):
            if r*(i_b) > i_a:
                c.append(a[i_a])
                i_a += 1
            c.append(b[i_b])
        return c

    res = lol[0]
    for i in lol[1:]:
        res = merge(res,i)
    return res

def test_riffle():
    def gen_taglist(prefix, amount):
        return [prefix+str(i) for i in xrange(amount)]

    def print_lst(lst):
        for x in lst:
            print x, " ",
        print

    for i in xrange(10):
        print_lst( riffle( [gen_taglist(chr(a+ord('a')),
                            random.randint(2,13))
                            for a in range( random.randint(1,10))] ))
        print

